# Message encoder / decoder
#### Video Demo:  https://youtu.be/7wz35LDrDNA
#### Gitlab: https://gitlab.com/sgreadly/cs50p-python-message-encoder-decoder

## Description:

For my CS50P final project, I created a message encoder / decoder. 

The python script can take a clear text message, either from a file, or by the user's input. It will then encode the text using a simple algorithm, jumbling it up into a 'secret' message, with an associated key. Both files are saved in binary form.

If a user obtains the message binary file, they would not be able to read its content without the key binary file. 

The script will also accept a message and key binary file, and decode the secret message back into clear text for the user to read. 


## Script parameters:

The script accepts 3 parameters: -h, -e, and -d

```
➜ python3 project.py
usage: project.py [-h] [-e [ENCODE] | -d DECODE DECODE]

CS50 Final Project - Message encoder / decoder

options:
  -h, --help            show this help message and exit
  -e [ENCODE], --encode [ENCODE]
                        message file to encode
  -d DECODE DECODE, --decode DECODE DECODE
                        message file to decode, as well as its key file

o_O
```

For example,

* To encode a message you want to input manually: 

```
python3 project.py -e
```

**Note:** there seems to be a bug with **sys.stdin.readlines()** where a single line needs CTRL-D 3 times.

* To encode a message you want to load from a file: 

```
python3 project.py -e message.txt
```

* To decode a message with its key:

```
python3 project.py -d encoded.bin key.bin
```


## Running the script: 

1. Encode a manually input message:

```
➜ python3 project.py -e                           
Type your message. Pres CTRL+D / CTRL+Z to finish

This is a message I want to encode so I can send it to my friend.

It consists of a few sentences. 

Thanks!
````

When done, press CTRL+D (Mac) or CTRL+Z (Windows). 

**Note**: there seems to be a bug with **sys.stdin.readlines()** where a single line needs CTRL-D 3 times.

Result: two files named **encoded.bin** and **key.bin**


2. Encode a text file

I have provided an example **message.txt* text file you can use to encode:
```
cat message.txt
Bones of full fifty men lie strewn about its lair. 

So, brave knights, if you do doubt your courage or your strength, come no further, 
    for death awaits you all with nasty, big, pointy teeth.

~ Tim the Enchanter
```

```
➜ python3 project.py -e message.txt
Encoding message... Done.

-----------ENCODED MESSAGE-----------

  at
h. hai
y soyyinys
 ht in n nto aura i oBulsptoois,frilk n,dor t  es unaedan iteiovt Tforro,behlh,~. ctmeuuyf ro fht ncut, ebi tmfgbefetrEwwoe
rnlnrroa oemwaef   i g y stdi tc ttalgo t shS   leeu boa,r r
euh toguy

-----------END MESSAGE-----------

Writing encoded result to file: encoded.bin ...Done.

Writing encoded result to file: key.bin ...Done.
```

3. Decode a binary message and key file:

```
➜ python3 project.py -d encoded.bin key.bin
Decoding message... Done.

-------------MESSAGE-------------

Bones of full fifty men lie strewn about its lair.

So, brave knights, if you do doubt your courage or your strength, come no further,
    for death awaits you all with nasty, big, pointy teeth.

~ Tim the Enchanter

-----------END MESSAGE-----------
```



## How it works

### Class

I have an Encoder class that either

* stores the encrypted message and key (if available), or 
* stores the clear text message. 

I used dunderscore __ variables to keep these 'secure' enough so they do not get tampered with or overridden outside of the class.

I created two getters for the message and key respectively. 

### main()

The main function simply controls and manipulates the other functions in the script. It will 

* Check the argv parameters to see if we are encoding or decoding, 
  - If encoding, will further check if we are encoding by manual input, or via a text file. 
  - If decoding, it will ask for the encoded message and key binary files and pass them to be decoded.


### Arguments

I started off with a larger piece of code to handle the argv arguments based on the lecture presentation, but making use of Python's library, I came across Argparse which makes argument handling very easy. 

https://docs.python.org/3/library/argparse.html

I added some protection against opening non text-basd files 

```
➜ python3 project.py -e cat1.gif
Error: not a utf-8 text file
```


### Encoding

I used a simple algorithm to encode/decode, by jumbling up the indexes for the message string, and storing a copy of both the jumbled message, and jumbled indexes. 

The 'key' is the list of jumbled indexes. 

For example, below is a message with 4 characters (for simplicity sake), along with its index

```
a  b  c  d
0  1  2  3

```

I use **random.shuffle()** to shuffle the indexes around. For example, the result may turn out as such:

```
c  a  d  b
2  0  3  1 
```

The string (c a d b) will be stored into its **encoded.bin** file, while the key (2 0 3 1) into its **key.bin** file.


### Decoding

Decoding is simply the inverse of the encoding. This is done in decode_message() function.

We have the jumbled string and key file. The trick is to aggregate the elements of both message & key lists into a tuple using zip(), then sort() the result based on the index, while appending the message into a **decoded** variable. 

```
c  a  d  b    jumbled message
0  1  2  3    indes of variable holding it
|  |  |  |
2  0  3  1    the key to sort by. Result: 'a b c d'
```

* What's above key 0 will be the first character to be displaied, i.e., 'a'
* What's above key 1 will be next in line, i.e., 'b'
* then key 2, which is 'c'
* then key 3, which is 'd'


### Saving to file

Done via **save_message()** function.

I started out with a longer and more manual method of saving the file, but I recall David M's comment of making use of Python's extensive library. 

I came across pickle, which simplifies the process of reading and writing the files.

https://docs.python.org/3/library/pickle.html

Thus, to write / save the encoded message to file, I use pk.dump(message, filename, 'wb')


### Reading from file

Opening a file is done via get_file(). It has the filename as a parameter, as well as a mode (b for binary, or t for text). 

I again used **pk.load()** onto the **open** file object.

> pickle.load(file, *, fix_imports=True, encoding='ASCII', errors='strict', buffers=None)


