from project import Encoder, get_args, get_file, encode_message, decode_message, save_message
import pytest
import argparse
import sys
import pickle as pk

def main():
    test_get_file()
    test_get_file_bad()
    test_encode_message()
    test_save_message()
    test_class_message()
    test_class_message_key()
    test_get_args()
    test_decode_message()
    

def test_class_message():
    foo = Encoder('Testing')    
    assert foo.message == 'Testing'
    
def test_class_message_key():
    bar = Encoder('TestTwo', '0123456')
    assert bar.message == 'TestTwo'
    assert bar.key == '0123456'
    
def test_get_args():
    sys.argv[1] = '-e'
    action = get_args(sys.argv[1])
    assert action.encode == '_manual_input_'

def test_get_file():
    text_msg = get_file('short.txt', 't')
    assert text_msg.message == 'ABCD'
    
def test_get_file_bad():
    with pytest.raises(FileNotFoundError):
        get_file('non_existant_file.txt', 't')

def test_encode_message():
    enc_msg, enc_key = encode_message('ABCD')
    assert len(enc_msg) == 4
    assert len(enc_key) == 4
    assert enc_msg and enc_key

def test_decode_message():
    assert decode_message(['C','A','B','D'], [2,0,1,3]) == 'ABCD'
    
def test_save_message():

    save_message('Testing', 'test_save.bin')
    
    # open the file, pk.load() it and check it saved with the above content
    test_file = open('test_save.bin', 'rb')
    result_file = pk.load(test_file)
    test_file.close()
    assert result_file == 'Testing'
    

if __name__ == "__main__":
    main()

