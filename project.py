import sys
import argparse
import pickle as pk
import random


class Encoder:
    '''
    Stores the clear text, or encoded message and key "privately"
    
    Attributes
    ----------
    message : str
        a string containing the clear text message to encode, or encoded message
    
    key : int
        a list of int() indexes of the jumbled up message
        if empty, then message will contain clear text
        
    
    Methods
    -------
    message(self)
        return the message
        
    key(self)
        return the key
    
    '''
    
    def __init__(self, message, key=None):
        
        self.__message = message
        if key:
            self.__key = key
    
    def __str__(self):
        '''If we have a decoded message, print it out'''
        if hasattr(self, 'decoded_message'):
            return f'{self.decoded_message}'
    
    
    #Getter
    @property
    def message(self):
        return self.__message
    
    #Getter
    @property
    def key(self):
        return self.__key
    


def main():
    
    # get argv arguments from CLI
    action = get_args(sys.argv[1:])
    
    # If we are encoding clear text, proceed: 
    if action.encode:
        
        # If we are encoding clear text inputted manually: 
        if action.encode == '_manual_input_':
            my_encoder = get_input()
        # if we are encoding clear text from a file: 
        else:
            try:
                my_encoder = get_file(action.encode, 't')
            except UnicodeDecodeError:
                sys.exit('Error: not a utf-8 text file')
        
        
        print(f'-----------MESSAGE-----------\n{"".join(my_encoder.message)}\n-----------END MESSAGE-----------\n')
        
        # encode the above message, into a message and key variable in our object
        my_encoder.encoded_message, my_encoder.encoded_key = encode_message(my_encoder.message)
        
        print(f'-----------ENCODED MESSAGE-----------\n{"".join(my_encoder.encoded_message)}\n-----------END MESSAGE-----------\n')
        
        # save encoded message/key into their respective files
        save_message(my_encoder.encoded_message, 'encoded.bin')
        save_message(my_encoder.encoded_key, 'key.bin')
    
    # If we are decoding a binary message & key file, proceed:        
    elif action.decode:
        # open message and key binary files and get their contents
        my_encoder = get_file(action.decode, 'b')
        
        # decode the message & key files
        my_encoder.decoded_message = decode_message(my_encoder.message, my_encoder.key)
                
        # print the resulted decoded message
        print(f'----------DECODED MESSAGE----------\n{my_encoder}\n------------END MESSAGE------------\n')
           


def get_args(argv):

    ''' Accept either -e to encode, or -d to decode
    
    Uses Argparse
    
    -e accepts 0 arguments (so you type the message until EOF) or 1 arg (the clear text file)
    -d requires 2 args: the encoded message file, and key file, in that order
    
    :param argv: a list that contains all the command-line arguments passed to the script
    :type argv: list
    :raise AttributeError: if no attributes were inputted
    :return: args: the arguments inputed, be it encode, decode, and filenames.
    :rtype: str
    '''
    
    parser = argparse.ArgumentParser(
        prog = 'project.py',
        description = 'CS50 Final Project - Message encoder / decoder',
        epilog = 'o_O'
    )
    # either -e, or -d, but cannot have both at same time
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-e', '--encode', nargs='?', const='_manual_input_',
                        help='message file to encode')
    group.add_argument('-d', '--decode', nargs=2,
                        help='message file to decode, as well as its key file')
    
    try:
        args = parser.parse_args(args=None if argv else ['--help'])
        return args
    except argparse.ArgumentError:
        sys.exit('Error, exiting.')
    except AttributeError:
        # if no attributes, print the help menu
        sys.exit(parser.print_help())



# Get input from user typing it in, until EOF
def get_input():
    ''' Receive the clear text input from the user's stdin
    
        Note there's a bug when inputting only 1 line with sys.stdin.readlines()
        where you need to press CTRL+D 3 times for it to accept.
        
        Workaround is to enter your 1 line, hit Enter, then CTRL+D
        
        :param None
        :raise KeyboardInterrupt: if you CTRL+C to cancel
        :return: A constructor for our Encoder class to create an object with the clear text message
        :rtype: object
    '''
    
    try:
        print('Type your message. Pres CTRL+D / CTRL+Z to finish\n')
        return Encoder(''.join(sys.stdin.readlines()))
    except KeyboardInterrupt:
        sys.exit('\nCancelled. Quitting.')



def get_file(msg_file, mode):
    '''
    Open needed files, be it clear text message file, or encoded message + key files
    
    Accepts filname and a mode of either 'b' for binary or 't' for text
    
    if 'b', then open the encoded message and key
    
    if 't', then open the clear text message
    
    Pass either of the above to Encoder class to create our object.
    
    :param msg_file: the filename to open
    :type msg_file: str
    :param mode: the mode for file read, either 'b' for binary or 't' for text
    :type mode: str
    :raise None
    :return: Constructed object from our Encoder class
    :rtype: object
    
    '''    
    
    # if mode is binary
    if mode == 'b':
        # open the encoded message binary file
        
        msg_enc = open(msg_file[0], 'rb')
        result_msg = pk.load(msg_enc)
        msg_enc.close()
        
        # open the encoded key binary file
        
        key_enc = open(msg_file[1], 'rb')
        result_key = pk.load(key_enc)
        key_enc.close()
        
        # construct our object consisting of encoded message and key
        return Encoder(result_msg, result_key)
        
        
    # else if mode is text
    elif mode == 't':
        with open(msg_file) as file:
            result = file.read()
        # construct our object consisting of clear text
        return Encoder(result)



def encode_message(msg):
    '''Encode the message
    
    Encode the message by shuffling the indexes around
    
    :param msg: the clear text message to encode
    :type msg: str
    :raise None
    :return: encoded_msg: the encoded message
    :rtype encoded_msg: str
    :return: sequence: the list of indexes (encode key)
    :rtype sequence: list
    '''
    
    sequence = []

    # get the index keys from the message and store into a list
    for i in range(0, len(msg)):
        sequence.append(i)

    # randomly shuffle the key list
    random.shuffle(sequence)

    print('Encoding message... ', end='')

    # shuffle the message using the above jumbled key 
    encoded_msg = [] 
    for j in range(len(msg)): 
        encoded_msg.append(msg[sequence[j]]) 

    print('Done.\n')
    
    return encoded_msg, sequence



def decode_message(message, key):
    '''Decode the message using its key
    
    Decode the message by un-shuffling the message string based on its shuffled index (key)
    
    Uses zip() to aggregate each character with its index from key, 
    then sorts by key, then prints the character
    
    :param message: the encoded message
    :type message: str
    :param key: the list of jumbled indexes to sort by
    :type key: list
    :raise None
    :return: decoded: the decoded message
    :rtype: str
    
    '''
    
    decoded = []
    print('Decoding message... ', end='')
    
    # zip the encoded list and key list together
    # sort the new list using sorted()
    # get the first element of the encrypted part, based on the sorted key
    for pair in sorted(zip(key, message)):
        letter = pair[1]
        decoded.append(letter) 

    print('Done.\n')
    return ''.join(decoded)
    
    

def save_message(encoded_text, filename):
    ''' Save an encoded message into a binary file
    
    Uses Pickle https://docs.python.org/3/library/pickle.html 
    
    Writes the encoded message into a file as binary format
    
    :param encoded_text: the encoded text to write to file
    :type encoded_text: str
    :param filename: the filname to save it to
    :type filename: str
    :raise pk.PicklingError: if there's an error writing 
    :return: None
    :rtype: None
    
    '''
    
    print(f'Writing encoded result to file: {filename} ...', end='')
    try:
        pk.dump(encoded_text, open(filename, 'wb'))
        print('Done.\n')
    except pk.PicklingError:
        sys.exit(f'Error writing to {filename}')
            


if __name__ == "__main__":
    main()